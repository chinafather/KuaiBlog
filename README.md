快博 - 基于.NET/Mono 的Blog程序
===================================
### 关于  
快博（KuaiBlog）是一个可以运行于.NET与Mono平台的博客程序。  
支持多用户  
在线一键安装  
免费模板下载  
后台一键更新  
  
该项目的数据库是在blogengine.NET 2.9的基础上进行调整的，基本兼容（目的是为了方便我自己的blog迁移）！  
  
###介绍  
快博UI层使用jntemplate模板引擎来呈现，后端使用纯静态页，通过ajax进行数据交互。  
本项目同时用到以下开源组件（或者库）  
├ jntemplate 后端模板引擎  
├ najax 后端AJAX组件  
├ bootstrap 前端CSS组件  
├ jquery 前端JS库  
└ umditor 富文本编辑器，ueditor的mini版  
  
### 结构  
KuaiBlog目录结构说明  
├ KuaiBlog 核心代码  
├ KuaiBlog.Admin 后台代码  
├ KuaiBlog.Entity 实体类  
├ KuaiBlog.SQLServerDAL 数据库操作类  
└ KuaiBlog.Web UI层  
　　├ admin 管理目录  
　　└ themes 主题目录  
　　　　└default 默认主题  

### 使用  
前台界面基本成型，更改目录下的themes的文件，即可修改风格！  
数据库可以直接使用blogengine.NET 2.9的数据库。执行一下sql scripts目录下的blogengine.net 2.9 to kuaiblog.sql文件即可！  
或者手动执行database v1.0.sql创建数据库  
后期会支持一键安装功能及sqlite数据库  
