﻿
using System;
namespace JinianNet.KuaiBlog.Entity
{
	/// <summary>
	/// Referrers:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Referrer
	{
		public Referrer()
		{}
		#region Model
		private int _referrerrowid;
		private Guid _blogid;
		private Guid _referrerid;
		private DateTime _referralday= DateTime.Now;
		private string _referrerurl;
		private int _referralcount;
		private string _url;
		private bool _isspam;
		/// <summary>
		/// 
		/// </summary>
		public int ReferrerRowId
		{
			set{ _referrerrowid=value;}
			get{return _referrerrowid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid BlogId
		{
			set{ _blogid=value;}
			get{return _blogid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid ReferrerId
		{
			set{ _referrerid=value;}
			get{return _referrerid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime ReferralDay
		{
			set{ _referralday=value;}
			get{return _referralday;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ReferrerUrl
		{
			set{ _referrerurl=value;}
			get{return _referrerurl;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int ReferralCount
		{
			set{ _referralcount=value;}
			get{return _referralcount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Url
		{
			set{ _url=value;}
			get{return _url;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsSpam
		{
			set{ _isspam=value;}
			get{return _isspam;}
		}
		#endregion Model

	}
}

