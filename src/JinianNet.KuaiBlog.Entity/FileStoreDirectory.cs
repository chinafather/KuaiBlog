﻿
using System;
namespace JinianNet.KuaiBlog.Entity
{
	/// <summary>
	/// FileStoreDirectory:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class FileStoreDirectory
	{
		public FileStoreDirectory()
		{}
		#region Model
		private Guid _id;
		private Guid _parentid;
		private Guid _blogid;
		private string _name;
		private string _fullpath;
		private DateTime _createdate;
		private DateTime _lastaccess;
		private DateTime _lastmodify;
		/// <summary>
		/// 
		/// </summary>
		public Guid Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid ParentId
		{
			set{ _parentid=value;}
			get{return _parentid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid BlogId
		{
			set{ _blogid=value;}
			get{return _blogid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Name
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string FullPath
		{
			set{ _fullpath=value;}
			get{return _fullpath;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime CreateDate
		{
			set{ _createdate=value;}
			get{return _createdate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime LastAccess
		{
			set{ _lastaccess=value;}
			get{return _lastaccess;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime LastModify
		{
			set{ _lastmodify=value;}
			get{return _lastmodify;}
		}
		#endregion Model

	}
}

