﻿
using System;
namespace JinianNet.KuaiBlog.Entity
{
	/// <summary>
	/// Rights:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Right
	{
		public Right()
		{}
		#region Model
		private int _rightrowid;
		private Guid _blogid;
		private string _rightname;
		/// <summary>
		/// 
		/// </summary>
		public int RightRowId
		{
			set{ _rightrowid=value;}
			get{return _rightrowid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid BlogId
		{
			set{ _blogid=value;}
			get{return _blogid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string RightName
		{
			set{ _rightname=value;}
			get{return _rightname;}
		}
		#endregion Model

	}
}

