﻿
using System;
namespace JinianNet.KuaiBlog.Entity
{
	/// <summary>
	/// Profiles:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Profile
	{
		public Profile()
		{}
		#region Model
		private int _profileid;
		private Guid _blogid;
		private string _username;
		private string _settingname;
		private string _settingvalue;
		/// <summary>
		/// 
		/// </summary>
		public int ProfileId
		{
			set{ _profileid=value;}
			get{return _profileid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid BlogId
		{
			set{ _blogid=value;}
			get{return _blogid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UserName
		{
			set{ _username=value;}
			get{return _username;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SettingName
		{
			set{ _settingname=value;}
			get{return _settingname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SettingValue
		{
			set{ _settingvalue=value;}
			get{return _settingvalue;}
		}
		#endregion Model

	}
}

