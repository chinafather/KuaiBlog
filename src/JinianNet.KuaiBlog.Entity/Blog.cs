﻿
using System;
namespace JinianNet.KuaiBlog.Entity
{
	/// <summary>
	/// Blogs:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Blog
	{
		public Blog()
		{}
		#region Model
		private int _blogrowid;
		private Guid _blogid;
		private string _blogname;
		private string _hostname;
		private bool _isanytextbeforehostnameaccepted;
		private string _storagecontainername;
		private string _virtualpath;
		private bool _isprimary;
		private bool _isactive;
		private bool _issiteaggregation= false;
		/// <summary>
		/// 
		/// </summary>
		public int BlogRowId
		{
			set{ _blogrowid=value;}
			get{return _blogrowid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid BlogId
		{
			set{ _blogid=value;}
			get{return _blogid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string BlogName
		{
			set{ _blogname=value;}
			get{return _blogname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Hostname
		{
			set{ _hostname=value;}
			get{return _hostname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsAnyTextBeforeHostnameAccepted
		{
			set{ _isanytextbeforehostnameaccepted=value;}
			get{return _isanytextbeforehostnameaccepted;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string StorageContainerName
		{
			set{ _storagecontainername=value;}
			get{return _storagecontainername;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string VirtualPath
		{
			set{ _virtualpath=value;}
			get{return _virtualpath;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsPrimary
		{
			set{ _isprimary=value;}
			get{return _isprimary;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsActive
		{
			set{ _isactive=value;}
			get{return _isactive;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsSiteAggregation
		{
			set{ _issiteaggregation=value;}
			get{return _issiteaggregation;}
		}
		#endregion Model

	}
}

