﻿
using System;
namespace JinianNet.KuaiBlog.Entity
{
	/// <summary>
	/// FileStoreFileThumbs:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class FileStoreFileThumb
	{
		public FileStoreFileThumb()
		{}
		#region Model
		private Guid _thumbnailid;
		private Guid _fileid;
		private int _size;
		private byte[] _contents;
		/// <summary>
		/// 
		/// </summary>
		public Guid thumbnailId
		{
			set{ _thumbnailid=value;}
			get{return _thumbnailid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid FileId
		{
			set{ _fileid=value;}
			get{return _fileid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int size
		{
			set{ _size=value;}
			get{return _size;}
		}
		/// <summary>
		/// 
		/// </summary>
		public byte[] contents
		{
			set{ _contents=value;}
			get{return _contents;}
		}
		#endregion Model

	}
}

