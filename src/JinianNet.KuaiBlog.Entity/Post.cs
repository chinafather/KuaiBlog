﻿
using System;
namespace JinianNet.KuaiBlog.Entity
{
	/// <summary>
	/// Posts:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Post
	{
		public Post()
		{}
		#region Model
		private int _postrowid;
		private Guid _blogid;
		private Guid _postid;
		private string _title;
		private string _description;
		private string _postcontent;
		private DateTime _datecreated;
		private DateTime? _datemodified;
		private string _author;
		private bool _ispublished;
		private bool _iscommentenabled;
		private int? _raters;
		private decimal? _rating;
		private string _slug;
		private bool _isdeleted= false;
		/// <summary>
		/// 
		/// </summary>
		public int PostRowId
		{
			set{ _postrowid=value;}
			get{return _postrowid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid BlogId
		{
			set{ _blogid=value;}
			get{return _blogid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid PostId
		{
			set{ _postid=value;}
			get{return _postid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Title
		{
			set{ _title=value;}
			get{return _title;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Description
		{
			set{ _description=value;}
			get{return _description;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string PostContent
		{
			set{ _postcontent=value;}
			get{return _postcontent;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime DateCreated
		{
			set{ _datecreated=value;}
			get{return _datecreated;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? DateModified
		{
			set{ _datemodified=value;}
			get{return _datemodified;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Author
		{
			set{ _author=value;}
			get{return _author;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsPublished
		{
			set{ _ispublished=value;}
			get{return _ispublished;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsCommentEnabled
		{
			set{ _iscommentenabled=value;}
			get{return _iscommentenabled;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Raters
		{
			set{ _raters=value;}
			get{return _raters;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? Rating
		{
			set{ _rating=value;}
			get{return _rating;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Slug
		{
			set{ _slug=value;}
			get{return _slug;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsDeleted
		{
			set{ _isdeleted=value;}
			get{return _isdeleted;}
		}
		#endregion Model

	}
}

