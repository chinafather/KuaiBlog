﻿
using System;
namespace JinianNet.KuaiBlog.Entity
{
	/// <summary>
	/// PostComment:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class PostComment
	{
		public PostComment()
		{}
		#region Model
		private int _postcommentrowid;
		private Guid _blogid;
		private Guid _postcommentid;
		private Guid _postid;
		private Guid _parentcommentid;
		private DateTime _commentdate;
		private string _author;
		private string _email;
		private string _website;
		private string _comment;
		private string _country;
		private string _ip;
		private bool _isapproved;
		private string _moderatedby;
		private string _avatar;
		private bool _isspam= false;
		private bool _isdeleted= false;
		/// <summary>
		/// 
		/// </summary>
		public int PostCommentRowId
		{
			set{ _postcommentrowid=value;}
			get{return _postcommentrowid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid BlogId
		{
			set{ _blogid=value;}
			get{return _blogid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid PostCommentId
		{
			set{ _postcommentid=value;}
			get{return _postcommentid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid PostId
		{
			set{ _postid=value;}
			get{return _postid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid ParentCommentId
		{
			set{ _parentcommentid=value;}
			get{return _parentcommentid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime CommentDate
		{
			set{ _commentdate=value;}
			get{return _commentdate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Author
		{
			set{ _author=value;}
			get{return _author;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Email
		{
			set{ _email=value;}
			get{return _email;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Website
		{
			set{ _website=value;}
			get{return _website;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Comment
		{
			set{ _comment=value;}
			get{return _comment;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Country
		{
			set{ _country=value;}
			get{return _country;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Ip
		{
			set{ _ip=value;}
			get{return _ip;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsApproved
		{
			set{ _isapproved=value;}
			get{return _isapproved;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ModeratedBy
		{
			set{ _moderatedby=value;}
			get{return _moderatedby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Avatar
		{
			set{ _avatar=value;}
			get{return _avatar;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsSpam
		{
			set{ _isspam=value;}
			get{return _isspam;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsDeleted
		{
			set{ _isdeleted=value;}
			get{return _isdeleted;}
		}
		#endregion Model

	}
}

