﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;

namespace JinianNet.KuaiBlog.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:CustomFields
	/// </summary>
    public partial class CustomField : BaseDAL
	{
		public CustomField()
		{}
		



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(JinianNet.KuaiBlog.Entity.CustomField model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into be_CustomFields(");
			strSql.Append("CustomType,ObjectId,BlogId,Key,Value,Attribute)");
			strSql.Append(" values (");
			strSql.Append("@CustomType,@ObjectId,@BlogId,@Key,@Value,@Attribute)");
			SqlParameter[] parameters = {
					new SqlParameter("@CustomType", SqlDbType.NVarChar,25),
					new SqlParameter("@ObjectId", SqlDbType.NVarChar,100),
					new SqlParameter("@BlogId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@Key", SqlDbType.NVarChar,150),
					new SqlParameter("@Value", SqlDbType.NVarChar,-1),
					new SqlParameter("@Attribute", SqlDbType.NVarChar,250)};
			parameters[0].Value = model.CustomType;
			parameters[1].Value = model.ObjectId;
			parameters[2].Value = Guid.NewGuid();
			parameters[3].Value = model.Key;
			parameters[4].Value = model.Value;
			parameters[5].Value = model.Attribute;

			int rows=Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(JinianNet.KuaiBlog.Entity.CustomField model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update be_CustomFields set ");
			strSql.Append("CustomType=@CustomType,");
			strSql.Append("ObjectId=@ObjectId,");
			strSql.Append("BlogId=@BlogId,");
			strSql.Append("Key=@Key,");
			strSql.Append("Value=@Value,");
			strSql.Append("Attribute=@Attribute");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
					new SqlParameter("@CustomType", SqlDbType.NVarChar,25),
					new SqlParameter("@ObjectId", SqlDbType.NVarChar,100),
					new SqlParameter("@BlogId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@Key", SqlDbType.NVarChar,150),
					new SqlParameter("@Value", SqlDbType.NVarChar,-1),
					new SqlParameter("@Attribute", SqlDbType.NVarChar,250)};
			parameters[0].Value = model.CustomType;
			parameters[1].Value = model.ObjectId;
			parameters[2].Value = model.BlogId;
			parameters[3].Value = model.Key;
			parameters[4].Value = model.Value;
			parameters[5].Value = model.Attribute;

			int rows=Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from be_CustomFields ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			int rows=Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public JinianNet.KuaiBlog.Entity.CustomField GetItem()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 CustomType,ObjectId,BlogId,Key,Value,Attribute from be_CustomFields ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			JinianNet.KuaiBlog.Entity.CustomField model=new JinianNet.KuaiBlog.Entity.CustomField();
			DataTable dt=Helper.ExecuteTable(CommandType.Text,strSql.ToString(),parameters);
			if(dt.Rows.Count>0)
			{
				return DataRowToModel(dt.Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public JinianNet.KuaiBlog.Entity.CustomField DataRowToModel(DataRow row)
		{
			JinianNet.KuaiBlog.Entity.CustomField model=new JinianNet.KuaiBlog.Entity.CustomField();
			if (row != null)
			{
				if(row["CustomType"]!=null)
				{
					model.CustomType=row["CustomType"].ToString();
				}
				if(row["ObjectId"]!=null)
				{
					model.ObjectId=row["ObjectId"].ToString();
				}
				if(row["BlogId"]!=null && row["BlogId"].ToString()!="")
				{
					model.BlogId= new Guid(row["BlogId"].ToString());
				}
				if(row["Key"]!=null)
				{
					model.Key=row["Key"].ToString();
				}
				if(row["Value"]!=null)
				{
					model.Value=row["Value"].ToString();
				}
				if(row["Attribute"]!=null)
				{
					model.Attribute=row["Attribute"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataTable GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select CustomType,ObjectId,BlogId,Key,Value,Attribute ");
			strSql.Append(" FROM be_CustomFields ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return Helper.ExecuteTable(CommandType.Text,strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataTable GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" CustomType,ObjectId,BlogId,Key,Value,Attribute ");
			strSql.Append(" FROM be_CustomFields ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return Helper.ExecuteTable(CommandType.Text,strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM be_CustomFields ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = Helper.ExecuteScalar(CommandType.Text,strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataTable GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.CategoryRowId desc");
			}
			strSql.Append(")AS Row, T.*  from be_CustomFields T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return Helper.ExecuteTable(CommandType.Text,strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataTable GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "be_CustomFields";
			parameters[1].Value = "CategoryRowId";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		
		

		
	}
}

